FROM rclone/rclone:1.56.2
RUN apk add --no-cache postgresql-client mongodb-tools
ENTRYPOINT []
